#include <memory>
#include <rclcpp/rclcpp.hpp>
#include <geometry_msgs/msg/twist.hpp>
#include <geometry_msgs/msg/pose.hpp>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2/LinearMath/Quaternion.h>
#include <geometry_msgs/msg/transform_stamped.hpp>
#include <tf2/LinearMath/Matrix3x3.h>
#include <std_msgs/msg/float64.hpp>
#include <std_msgs/msg/int32.hpp>
#include <sensor_msgs/msg/joint_state.hpp>
#include <tf2_geometry_msgs/tf2_geometry_msgs.hpp>
#include <std_msgs/msg/bool.hpp>
#include <std_msgs/msg/string.hpp>
#include <nav_msgs/msg/path.hpp>

class BaseLinkController : public rclcpp::Node
{

    // Status of the loading process
    enum class LoadingState
    {
        IDLE,
        MOVING_ARM_1,
        MOVING_BUCKET,
        MOVING_FORWARD,
        MOVING_ARM_2,
        COMPLETED
    };

    LoadingState loading_state_ = LoadingState::IDLE;
    rclcpp::TimerBase::SharedPtr loading_timer_;

    // Status of the unloading process
    enum class UnloadingState
    {
        IDLE,
        MOVING_FORWARD,
        MOVING_ARM,
        MOVING_BUCKET,
        COMPLETED
    };

    UnloadingState unloading_state_ = UnloadingState::IDLE;
    rclcpp::TimerBase::SharedPtr unloading_timer_;

    rclcpp::Time last_time_;
    nav_msgs::msg::Path path_;
    double x_, y_, theta_;
    int selected_marker_id = -1; // Initialized to -1 to indicate that there are no selected markers

    rclcpp::Subscription<std_msgs::msg::Int32>::SharedPtr selected_marker_sub;

    rclcpp::Subscription<geometry_msgs::msg::Pose>::SharedPtr target_position_subscription_;

    rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr cmd_vel_publisher_;

    // rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr joint_state_subscription_;

    // rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr processed_joint_state_publisher_;

    // Notification of change in publication status
    rclcpp::Publisher<geometry_msgs::msg::Pose>::SharedPtr model_update_publisher_;

    rclcpp::Subscription<std_msgs::msg::Bool>::SharedPtr load_unload_subscription_;

    rclcpp::Publisher<std_msgs::msg::Float64>::SharedPtr arm_publisher_;
    rclcpp::Publisher<std_msgs::msg::Float64>::SharedPtr bucket_publisher_;
    rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr forward_publisher_;

    rclcpp::Publisher<std_msgs::msg::String>::SharedPtr pickup_target_publisher_;

    rclcpp::Publisher<nav_msgs::msg::Path>::SharedPtr path_publisher_;

public:
    BaseLinkController() : Node("base_link_controller"), last_time_(this->now()), x_(0.0), y_(0.0), theta_(0.0)
    {
        RCLCPP_INFO(this->get_logger(), "Position: (%f, %f), Orientation: %f", x_, y_, theta_);
        subscription_ = this->create_subscription<geometry_msgs::msg::Twist>(
            "cmd_vel", 10, std::bind(&BaseLinkController::cmd_vel_callback, this, std::placeholders::_1));

        target_position_subscription_ = this->create_subscription<geometry_msgs::msg::Pose>(
            "target_position", 10, std::bind(&BaseLinkController::target_position_callback, this, std::placeholders::_1));

        cmd_vel_publisher_ = this->create_publisher<geometry_msgs::msg::Twist>("cmd_vel", 10);

        // Initializing the TF Broadcaster
        tf_broadcaster_ = std::make_unique<tf2_ros::TransformBroadcaster>(*this);

        // 初始化订阅者以接收装卸货更新的通知
        load_unload_subscription_ = this->create_subscription<std_msgs::msg::Bool>(
            "load_updated", 10, std::bind(&BaseLinkController::load_update_callback, this, std::placeholders::_1));

        arm_publisher_ = this->create_publisher<std_msgs::msg::Float64>("/arm_command", 10);
        bucket_publisher_ = this->create_publisher<std_msgs::msg::Float64>("/bucket_command", 10);
        forward_publisher_ = this->create_publisher<geometry_msgs::msg::Twist>("/cmd_vel", 10);

        pickup_target_publisher_ = this->create_publisher<std_msgs::msg::String>("pickup_target", 10);

        // joint_state_subscription_ = this->create_subscription<sensor_msgs::msg::JointState>(
        //     "/joint_states", 10,
        //     std::bind(&BaseLinkController::joint_state_callback, this, std::placeholders::_1));

        // processed_joint_state_publisher_ = this->create_publisher<sensor_msgs::msg::JointState>("/joint_states", 10);

        // Initialize subscribers to receive notifications of loading/unloading updates
        model_update_publisher_ = this->create_publisher<geometry_msgs::msg::Pose>("model_updated", 10);

        path_publisher_ = this->create_publisher<nav_msgs::msg::Path>("path_track", 10);
        path_.header.frame_id = "world";

        selected_marker_sub = this->create_subscription<std_msgs::msg::Int32>(
            "/selected_marker", 10,
            std::bind(&BaseLinkController::selectedMarkerCallback, this, std::placeholders::_1));
    }

private:
    void cmd_vel_callback(const geometry_msgs::msg::Twist::SharedPtr msg)
    {
        auto now = this->now();
        // double delta_t = (now - last_time_).seconds();
        double delta_t = 0.1;
        last_time_ = now;

        // Calculate the velocity component after rotation
        double dx = msg->linear.x * cos(theta_) - msg->linear.y * sin(theta_);
        double dy = msg->linear.x * sin(theta_) + msg->linear.y * cos(theta_);

        x_ += dx * delta_t;
        y_ += dy * delta_t;
        theta_ += msg->angular.z * delta_t;

        // x_ += msg->linear.x * delta_t;
        // y_ += msg->linear.y * delta_t;
        // theta_ += msg->angular.z * delta_t;

        geometry_msgs::msg::TransformStamped transformStamped;
        transformStamped.header.stamp = now;
        transformStamped.header.frame_id = "world";
        transformStamped.child_frame_id = "base_link";
        transformStamped.transform.translation.x = x_;
        transformStamped.transform.translation.y = y_;
        transformStamped.transform.translation.z = 0.0;

        tf2::Quaternion q;
        q.setRPY(0, 0, theta_);
        transformStamped.transform.rotation.x = q.x();
        transformStamped.transform.rotation.y = q.y();
        transformStamped.transform.rotation.z = q.z();
        transformStamped.transform.rotation.w = q.w();

        tf_broadcaster_->sendTransform(transformStamped);

        // Constructs and publishes a message containing information about the current location
        geometry_msgs::msg::Pose update_msg;
        update_msg.position.x = x_;
        update_msg.position.y = y_;
        update_msg.orientation = tf2::toMsg(q);

        // Print the contents of update_msg
        // RCLCPP_INFO(this->get_logger(), "Publishing Update Message:");
        // RCLCPP_INFO(this->get_logger(), "  Position X: %f, Y: %f", update_msg.position.x, update_msg.position.y);
        // RCLCPP_INFO(this->get_logger(), "  Orientation: (%f, %f, %f, %f)",
        //             update_msg.orientation.x, update_msg.orientation.y,
        //             update_msg.orientation.z, update_msg.orientation.w);

        model_update_publisher_->publish(update_msg);

        // Adding a new location to a path
        geometry_msgs::msg::PoseStamped new_pose;
        new_pose.pose.position.x = x_;
        new_pose.pose.position.y = y_;
        new_pose.pose.orientation = tf2::toMsg(q);
        new_pose.header.stamp = now;
        new_pose.header.frame_id = "world";

        path_.poses.push_back(new_pose);

        // Publishing Path
        path_publisher_->publish(path_);
    }

    void target_position_callback(const geometry_msgs::msg::Pose::SharedPtr msg)
    {
        // Extract target location
        double target_x = msg->position.x;
        double target_y = msg->position.y;

        // Calculate the distance to the target location
        double distance_to_target = sqrt(pow(target_x - x_, 2) + pow(target_y - y_, 2));

        // Converting quaternions to Euler angles
        tf2::Quaternion q(msg->orientation.x, msg->orientation.y, msg->orientation.z, msg->orientation.w);
        tf2::Matrix3x3 m(q);
        double roll, pitch, yaw;
        m.getRPY(roll, pitch, yaw);

        // Calculate the angular difference
        double angle_to_target = atan2(target_y - y_, target_x - x_);
        double angle_diff = angle_to_target - theta_;
        angle_diff = atan2(sin(angle_diff), cos(angle_diff)); // Normalized to [-pi, pi]

        geometry_msgs::msg::Twist cmd_vel_msg;

        // If the angular difference is small enough and at a certain distance from the target, move in a straight line
        if (fabs(angle_diff) < 0.1 && distance_to_target > 2.3)
        {
            cmd_vel_msg.linear.x = std::min(distance_to_target, 0.5); // Limit maximum speed
            cmd_vel_msg.angular.z = 0.0;
        }
        // Otherwise, rotate to face the target direction
        else if (fabs(angle_diff) >= 0.1)
        {
            cmd_vel_msg.angular.z = std::min(std::max(angle_diff, -0.5), 0.5); // Limit maximum rotation speed
            cmd_vel_msg.linear.x = 0.0;
        }
        // Stop if it has reached the vicinity of the target
        else
        {
            // If close enough to the target and the operation has not been completed
            if (distance_to_target <= 2.3 && !operation_completed_)
            {
                if (load)
                {
                    // Perform loading operations
                    performLoading();
                }
                else
                {
                    // Perform unloading operations
                    performUnloading();
                }
                operation_completed_ = true; // Mark operation complete
            }
            cmd_vel_msg.linear.x = 0.0;
            cmd_vel_msg.angular.z = 0.0;
        }

        // publish control commands
        cmd_vel_publisher_->publish(cmd_vel_msg);
    }

    void load_update_callback(const std_msgs::msg::Bool::SharedPtr msg)
    {

        load = msg->data;

        if (operation_completed_)
        {
            operation_completed_ = false;
        }
    }

    void performLoading()
    {
        // Performs loading actions
        loading_state_ = LoadingState::MOVING_ARM_1; // Setting the initial state

        // Creating a timer to step through the loading process
        loading_timer_ = this->create_wall_timer(
            std::chrono::seconds(1), // interval time
            std::bind(&BaseLinkController::loadingStep, this));
    }

    void performUnloading()
    {
        // Performs unloading actions
        unloading_state_ = UnloadingState::MOVING_FORWARD; // Setting the initial state

        // Creating a timer to step through the unloading process
        unloading_timer_ = this->create_wall_timer(
            std::chrono::seconds(1), // interval time
            std::bind(&BaseLinkController::unloadingStep, this));
    }

    // void joint_state_callback(const sensor_msgs::msg::JointState::SharedPtr msg)
    // {
    //     // The received joint states are processed here
    //     sensor_msgs::msg::JointState processed_joint_state;
    //     processed_joint_state.header.stamp = this->now(); // Setting the current timestamp

    //     processed_joint_state.name = msg->name;
    //     processed_joint_state.position = msg->position;
    //     processed_joint_state.velocity = msg->velocity;
    //     processed_joint_state.effort = msg->effort;

    //     // Publish the status of the processed joints
    //     processed_joint_state_publisher_->publish(processed_joint_state);
    // }

    void unloadingStep()
    {
        switch (unloading_state_)
        {
        case UnloadingState::MOVING_FORWARD:
            moveForwardUnload();
            unloading_state_ = UnloadingState::MOVING_ARM;
            break;
        case UnloadingState::MOVING_ARM:
            moveArmUnload();
            unloading_state_ = UnloadingState::MOVING_BUCKET;
            break;
        case UnloadingState::MOVING_BUCKET:
            moveBucketUnload();
            unloading_state_ = UnloadingState::COMPLETED;
            break;
        case UnloadingState::COMPLETED:
            RCLCPP_INFO(this->get_logger(), "Unloading completed");
            unloading_timer_->cancel(); // Stop timer
            break;
        default:
            break;
        }
    }

    void moveForwardUnload()
    {
        // publish forward command
        auto forward_message_unLoad = geometry_msgs::msg::Twist();
        forward_message_unLoad.linear.x = 0.5;
        forward_message_unLoad.linear.y = 0.0;
        forward_message_unLoad.linear.z = 0.0;
        forward_message_unLoad.angular.x = 0.0;
        forward_message_unLoad.angular.y = 0.0;
        forward_message_unLoad.angular.z = 0.0;
        forward_publisher_->publish(forward_message_unLoad);
    }

    void moveArmUnload()
    {
        // publish the command of arms movement
        auto arm_msg_unLoad = std_msgs::msg::Float64();
        arm_msg_unLoad.data = -0.15;
        arm_publisher_->publish(arm_msg_unLoad);
    }

    void moveBucketUnload()
    {
        // publish the command of bucket movement
        auto bucket_msg_unLoad = std_msgs::msg::Float64();
        bucket_msg_unLoad.data = 0.89;
        bucket_publisher_->publish(bucket_msg_unLoad);
        RCLCPP_INFO(this->get_logger(), "Unloading completed");

        // publish unloading target to land
        if (selected_marker_id > 0)
        {
            // Convert integer ID to string
            std::string id_str = std::to_string(selected_marker_id);

            pickup_msg.data = "DROPIT" + id_str;
            RCLCPP_INFO(this->get_logger(), "Received drop command: %s", pickup_msg.data.c_str());
            pickup_target_publisher_->publish(pickup_msg);
        }
        else
        {
            RCLCPP_ERROR(this->get_logger(), "Invalid selected marker ID: %d", selected_marker_id);
        }
    }

    void loadingStep()
    {
        switch (loading_state_)
        {
        case LoadingState::MOVING_ARM_1:
            moveArmLoad_1();
            loading_state_ = LoadingState::MOVING_BUCKET;
            break;
        case LoadingState::MOVING_BUCKET:
            moveBucketLoad();
            loading_state_ = LoadingState::MOVING_FORWARD;
            break;
        case LoadingState::MOVING_FORWARD:
            moveForwardLoad();
            loading_state_ = LoadingState::MOVING_ARM_2;
            break;
        case LoadingState::MOVING_ARM_2:
            moveArmLoad_2();
            loading_state_ = LoadingState::COMPLETED;
            break;
        case LoadingState::COMPLETED:
            RCLCPP_INFO(this->get_logger(), "Loading completed");
            loading_timer_->cancel(); // Stop timer
            break;
        default:
            break;
        }
    }

    void moveForwardLoad()
    {
        // publish forward command
        auto forward_message_load = geometry_msgs::msg::Twist();
        forward_message_load.linear.x = 0.5;
        forward_message_load.linear.y = 0.0;
        forward_message_load.linear.z = 0.0;
        forward_message_load.angular.x = 0.0;
        forward_message_load.angular.y = 0.0;
        forward_message_load.angular.z = 0.0;
        forward_publisher_->publish(forward_message_load);

        // publish loading target attach to bucket
        if (selected_marker_id > 0)
        {
            // Convert integer ID to string
            std::string id_str = std::to_string(selected_marker_id);

            pickup_msg.data = "ATTACH" + id_str;
            RCLCPP_INFO(this->get_logger(), "Received attach command: %s", pickup_msg.data.c_str());
            pickup_target_publisher_->publish(pickup_msg);
        }
        else
        {
            RCLCPP_ERROR(this->get_logger(), "Invalid selected marker ID: %d", selected_marker_id);
        }
    }

    void moveArmLoad_1()
    {
        auto arm_msg_load_1 = std_msgs::msg::Float64();
        arm_msg_load_1.data = -0.1;
        arm_publisher_->publish(arm_msg_load_1);
    }

    void moveArmLoad_2()
    {
        auto arm_msg_load_2 = std_msgs::msg::Float64();
        arm_msg_load_2.data = -0.28;
        arm_publisher_->publish(arm_msg_load_2);
    }

    void moveBucketLoad()
    {
        auto bucket_msg_load = std_msgs::msg::Float64();
        bucket_msg_load.data = 0.62;
        bucket_publisher_->publish(bucket_msg_load);
    }

    void selectedMarkerCallback(const std_msgs::msg::Int32::SharedPtr msg)
    {
        selected_marker_id = msg->data;
    }

    double load;

    bool operation_completed_ = false;

    std::unique_ptr<tf2_ros::TransformBroadcaster> tf_broadcaster_;

    std_msgs::msg::String pickup_msg;

    rclcpp::Subscription<geometry_msgs::msg::Twist>::SharedPtr subscription_;
};

int main(int argc, char *argv[])
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<BaseLinkController>());
    rclcpp::shutdown();
    return 0;
}