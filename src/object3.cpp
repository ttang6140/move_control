#include <memory>
#include <unordered_map>
#include <rclcpp/rclcpp.hpp>
#include <visualization_msgs/msg/marker.hpp>
#include <std_msgs/msg/string.hpp>
#include <geometry_msgs/msg/pose.hpp>

using namespace std::chrono_literals;

class SimpleMarkerArrayNode : public rclcpp::Node
{
public:
    SimpleMarkerArrayNode() : Node("target_object_controller")
    {
        // Initialize per-marker publishers and timers
        for (int i = 0; i < 3; ++i)
        {
            std::string marker_topic = "visualization_marker" + std::to_string(i);
            marker_pubs_[i] = this->create_publisher<visualization_msgs::msg::Marker>(marker_topic, 10);

            std::string point_topic = "marker_point" + std::to_string(i+1);
            point_pubs_[i] = this->create_publisher<geometry_msgs::msg::Point>(point_topic, 10);

            timers_[i] = this->create_wall_timer(
                50ms, [this, i]() { this->publishMarker(i); });

            initializeMarker(i);
        }

        pickup_target_sub = this->create_subscription<std_msgs::msg::String>(
            "pickup_target", 10, std::bind(&SimpleMarkerArrayNode::pickupCallback, this, std::placeholders::_1));

        target_position_subscription_ = this->create_subscription<geometry_msgs::msg::Pose>(
            "target_position", 10, std::bind(&SimpleMarkerArrayNode::target_position_callback, this, std::placeholders::_1));

        action_pub_ = this->create_publisher<std_msgs::msg::String>("action_type", 10);

    }

private:
    void initializeMarker(int id)
    {
        visualization_msgs::msg::Marker marker;
        marker.header.frame_id = "world";
        marker.ns = "basic_shapes";
        marker.id = id;
        marker.type = visualization_msgs::msg::Marker::CUBE;
        marker.action = visualization_msgs::msg::Marker::ADD;

        marker.pose.position.x = 1.0 * id + 4;
        marker.pose.position.y = 4.0;
        marker.pose.position.z = 0.15;
        marker.pose.orientation.w = 1.0;

        marker.scale.x = 0.3;
        marker.scale.y = 0.3;
        marker.scale.z = 0.3;

        marker.color.r = 0.0f + 0.3f * id;
        marker.color.g = 1.0f - 0.3f * id;
        marker.color.b = 0.0f + 0.2f * id;
        marker.color.a = 1.0;

        markers_[id] = marker;

        publishMarkerPoint(id, marker.pose.position.x, marker.pose.position.y, marker.pose.position.z);

        // auto timer = this->create_wall_timer(
        //     50ms, [this, id](){
        //         this->publishMarker(id);
        //     }
        // );
        // timers_[id] = std::move(timer);


    }

    void publishMarker(int id)
    {
        if (markers_.find(id) != markers_.end())
        {
            auto &marker = markers_[id];
            marker.header.stamp = this->get_clock()->now();
            marker_pubs_[id]->publish(marker);
        }
    }

    void pickupCallback(const std_msgs::msg::String::SharedPtr msg)
    {
        std::string command = msg->data.substr(0, 6);
        int id = -1;

        if (msg->data.size() > 6)
        {
            try
            {
                id = std::stoi(msg->data.substr(6)) - 1;
            }
            catch (const std::exception &e)
            {
                RCLCPP_ERROR(this->get_logger(), "Error parsing marker ID: %s", e.what());
                return;
            }
        }

        if (id >= 0 && id < 3)
        {
            if (command == "ATTACH")
            {
                attachMarkerToBucket(id);
            }
            else if (command == "DROPIT")
            {
                placeMarkerOnGround(id);
            }
        }
        else
        {
            RCLCPP_ERROR(this->get_logger(), "Invalid marker ID: %d", id);
        }
    }

    void attachMarkerToBucket(int id)
    {
        if (markers_.find(id) != markers_.end())
        {
            auto &marker = markers_[id];
            marker.header.frame_id = "bucket";
            marker.pose.position.x = 0.5;
            marker.pose.position.y = 0.0;
            marker.pose.position.z = 0.3;

            std::string id_str_action = std::to_string(id);
            action_msg.data = "loadMarker" + id_str_action;
            action_pub_->publish(action_msg);
            RCLCPP_INFO(this->get_logger(), "Marker load: %s", action_msg.data.c_str());

        }
    }

    void placeMarkerOnGround(int id)
    {
        if (markers_.find(id) != markers_.end())
        {
            auto &marker = markers_[id];
            marker.header.frame_id = "world";
            marker.pose.position.x = target_x;
            marker.pose.position.y = target_y;
            marker.pose.position.z = 0.15;

            publishMarkerPoint(id, marker.pose.position.x, marker.pose.position.y, marker.pose.position.z);

            std::string id_str_action_1 = std::to_string(id);
            action_msg.data = "unLoadMarker" + id_str_action_1;
            action_pub_->publish(action_msg);
            RCLCPP_INFO(this->get_logger(), "Marker unload: %s", action_msg.data.c_str());
        }
    }

    void target_position_callback(const geometry_msgs::msg::Pose::SharedPtr msg)
    {
        target_x = msg->position.x;
        target_y = msg->position.y;
    }

    void publishMarkerPoint(int id, float x, float y, float z) {
        if (point_pubs_.find(id) != point_pubs_.end()) {
            auto point = geometry_msgs::msg::Point();
            point.x = x;
            point.y = y;
            point.z = z;
            point_pubs_[id]->publish(point);
        }
    }

    double target_x = 0.0;
    double target_y = 0.0;

    std_msgs::msg::String action_msg;

    std::unordered_map<int, visualization_msgs::msg::Marker> markers_;
    std::unordered_map<int, rclcpp::Publisher<visualization_msgs::msg::Marker>::SharedPtr> marker_pubs_;
    std::unordered_map<int, rclcpp::TimerBase::SharedPtr> timers_;
    rclcpp::Subscription<std_msgs::msg::String>::SharedPtr pickup_target_sub;
    rclcpp::Subscription<geometry_msgs::msg::Pose>::SharedPtr target_position_subscription_;
    std::unordered_map<int, rclcpp::Publisher<geometry_msgs::msg::Point>::SharedPtr> point_pubs_;
    rclcpp::Publisher<std_msgs::msg::String>::SharedPtr action_pub_;
};

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    auto node = std::make_shared<SimpleMarkerArrayNode>();
    rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}
