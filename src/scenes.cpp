#include <memory>
#include <rclcpp/rclcpp.hpp>
#include <visualization_msgs/msg/marker.hpp>

class MeshMarkerNode : public rclcpp::Node
{
public:
    MeshMarkerNode()
        : Node("mesh_marker_node")
    {
        marker_pub = this->create_publisher<visualization_msgs::msg::Marker>("visualization_marker", 10);
        timer_ = this->create_wall_timer(
            std::chrono::milliseconds(50),
            std::bind(&MeshMarkerNode::publishMarker, this));
    }

private:
    void publishMarker()
    {
        visualization_msgs::msg::Marker marker;
        marker.header.frame_id = "world";
        marker.header.stamp = this->get_clock()->now();

        marker.ns = "mesh_marker";
        marker.id = 1;
        marker.type = visualization_msgs::msg::Marker::MESH_RESOURCE;
        marker.action = visualization_msgs::msg::Marker::ADD;

        // Set the path to the mesh resource
        marker.mesh_resource = "package://cpp00-urdf/meshes/view.dae";
        marker.mesh_use_embedded_materials = true;

        marker.pose.position.x = 0.0;
        marker.pose.position.y = 0.0;
        marker.pose.position.z = 0.0;

        marker.pose.orientation.x = 0.0;
        marker.pose.orientation.y = 0.0;
        marker.pose.orientation.z = 0.0;
        marker.pose.orientation.w = 1.0;

        marker.scale.x = 1.0; 
        marker.scale.y = 1.0;
        marker.scale.z = 1.0;

        // marker.color.r = 1.0f; // red
        // marker.color.g = 0.0f;
        // marker.color.b = 0.0f;
        // marker.color.a = 0.8; 

        marker.lifetime = rclcpp::Duration(0, 0); // persistent

        marker_pub->publish(marker);
    }

    rclcpp::Publisher<visualization_msgs::msg::Marker>::SharedPtr marker_pub;
    rclcpp::TimerBase::SharedPtr timer_;
};

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<MeshMarkerNode>());
    rclcpp::shutdown();
    return 0;
}
