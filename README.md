# move_control
- This package is developed on ROS2-humble environment(cmake).
- Its functions including operate cat259D model movement, automated loading and unloading.
- Using web controllor to control model.

## How to run
- Make sure you've downloaded the package of cat259D model in your ROS2 workspace.<br>
- git clone https://git.rwth-aachen.de/ttang6140/cpp00-urdf.git<br>
- [cat259D model url](https://git.rwth-aachen.de/ttang6140/cpp00-urdf) 
- If you run ros2 project in __wsl2__, you need to install xLaunch server on your physical machine. And run following command to configure `$DISPLAY`. 
- `export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2}'):0`

```
# Open a new terminal window
# Install corresponding software package
sudo apt-get update
sudo apt-get install ros-humble-xacro
source /opt/ros/humble/setup.bash

# Install required package to establish bridge network between ros2 and web.
sudo apt update
sudo apt install ros-$ROS_DISTRO-rosbridge-suite
source /opt/ros/humble/setup.bash

# This will start rosbridge_websocket on port 9090 of the local WebSocket server.
ros2 run rosbridge_server rosbridge_websocket
```

```
# Open a new terminal window
# Remember to change the IP address in the index.html which loactes in the directory ~/your_workspace/move_control 
  to your own IP(Line **311**).
  Such as url: 'ws://your IP address:9090'
# You can check your IP address by using command "ifconfig"
# You can place the index.html in the root directory in any local directory, and run the following command in the placed directory.
# In this project structure, you can run the following command under the directory ~/your_workspace/move_control
python3 -m http.server

# Type this url on your browser http://localhost:8000/
```

```
# Next build this project
# Open a new terminal window
cd ~/your_workspace/src

# If you have coloned cpp00-urdf package, you can neglect the following installation of cpp00-urdf.
git clone https://git.rwth-aachen.de/ttang6140/cpp00-urdf.git
git clone https://git.rwth-aachen.de/ttang6140/move_control.git
cd ~/your_workspace
colcon build --packages-select cpp00-urdf
colcon build --packages-select move_control
source install/setup.bash
ros2 launch cpp00-urdf display.launch.py

# Refresh web controllor before launch to avoid browser cache!!!

# Now, you can use your web controller to control the model.
```

## Project structure

1. cpp00-urdf
    + Including urdf file of cat259D and required mesh resource.
    + Launch file in the launch directory.

2. move_control
    + **armControl.cpp**: rotation of arms on cat259D.
    + **bucketControl.cpp**: rotation of bucket on cat259D.
    + **move.cpp**: control cat259D model body move.
    + **object3.cpp**: control three markers on rviz2.
    + **scenes.cpp**: publish marker to add scenes on rviz2.
    
3. index.html
    + As a web controllor to control model.

## How to use web controllor to control model

1. Basic body movement and joints rotation
    * Click on top five blue buttons to control forward, backword, left, right and stop. 
    * Sliding blue slides, then click on purple buttons to control rotation of arms and bucket.
    * Type X,Y coordinate, and click on green button(Move to Position) to get the destination.
    
2. Manually controlled model loading and unloading of cargo(three cubic makers)
    * These three labels(Marker 1,2,3) will show the coordinates of the three markers on the ground in real time.
    * You can click on the green button(Select Marker1 or Select Marker2 or Select Marker1) to select the corresponding goods, then click on the blue button(load), it will go to the position of marker and pick it up. After that you need type new X,Y coordinate for the destination of marker and click on the blue button(unload) and the green button(Move to Position) to allow the model to unload the goods.

3. Automated controlled model loading and unloading of cargo(three cubic makers)
    * Click on orange button, then type coordinates of the destination of the three goods, cat259D model will automatically identify the three markers coordinates in rviz2 and unload them to the destination in order. Finally, cat259D model will back to the initial position.


![project screenshot](screenshot.jpg)
